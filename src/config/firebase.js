const firebase = require('firebase')

const app = firebase.initializeApp({    
  // Initialize Firebase

    apiKey: "AIzaSyCPsyhYg7LPA3417u-9EBiS3R3RlDZZaHE",
    authDomain: "fir-nodejs-2e5e5.firebaseapp.com",
    databaseURL: "https://fir-nodejs-2e5e5.firebaseio.com",
    projectId: "fir-nodejs-2e5e5",
    storageBucket: "fir-nodejs-2e5e5.appspot.com",
    messagingSenderId: "372776063007"
})


module.exports = app;