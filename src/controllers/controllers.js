const firebase = require('../config/firebase');

module.exports = {



    async firebasePost(req, res) {
        try {


            const titulo = req.body.title
            const conteudo = req.body.content
            const ref = firebase.firestore().collection('news').doc()
            await ref.set({
                title: titulo,
                content: conteudo
            })
            
            return res.json({
                data: 'postado com sucesso'
            })
        } catch (error) {
            console.error('Deu ruim post', error)
        }
    },


    async firebaseGet(req, res) {



        let dados = []
        try {

            const ref = firebase.firestore().collection('news')
            await ref.get().then((snapshot) => {
                snapshot.docs.forEach(doc => {
                    let news = doc.data()


                    dados.push({ _id: doc.id, ...news })


                })

                return res.json({
                    data: dados

                })
            })




        } catch (error) {
            console.error('Deu ruim no Get', error)
        }
    },

    async firebaseDelete(req, res) {
        try {
            const { id } = req.params;
            
            const ref = firebase.firestore().collection('news')
            await ref.doc(id).delete()
            return res.json({
                message: 'Usuario deletado'
            })

        } catch (error) {
            console.log('Deu ruim no delete', error)
        }
    },

    async firebaseUpdate(req, res) {
        const { id } = req.params;
        const titulo = req.body.title
        const conteudo = req.body.content
      
        const ref = firebase.firestore().collection('news')
        await ref.doc(id).update({
            title: titulo,
            content: conteudo

        })
        return res.json({
            message: "Atualizado"
        })

    },

    async firebaseRealTime(req, res){
        let dados = []
        try {


         const ref = firebase.firestore().collection('news')
           await ref.onSnapshot(snapshot => {
                 snapshot.docs.forEach(doc => {
                     let news = doc.data()
                     dados.push({ _id: doc.id, ...news })
                 })

                 console.log(dados)
                 console.log('--------------------------------')
                 res.json({
                     data: dados
                 })
             })
        }catch(error){
            console.error('Deu ruim realtime', error)
        }
    }

}