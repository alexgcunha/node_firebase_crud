const express = require('express')
const controllers = require('./controllers/controllers')

const router = express.Router()



router.post('/firebase', controllers.firebasePost);
router.get('/firebase', controllers.firebaseGet)
router.get('/firebase-realtime', controllers.firebaseRealTime)
router.delete('/firebase/:id', controllers.firebaseDelete)
router.put('/firebase/:id', controllers.firebaseUpdate)


module.exports = router;